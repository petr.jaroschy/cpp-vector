#pragma once
#include <cstdlib>

template <typename T>
class Array
{
public:
	Array()
	{
		head_ = nullptr;
		count_ = 0;
		capacity_ = 0;
	}

	Array(size_t size)
	{
		if (size > 0) head_ = static_cast<T*>(calloc(size, sizeof(T)));
		else head_ = nullptr;

		count_ = 0;
		capacity_ = size;
	}

	Array(Array<T> const& array)
	{
		capacity_ = array.capacity_;
		count_ = array.count_;

		if (array.capacity_ > 0) head_ = static_cast<T*>(calloc(capacity_, sizeof(T)));
		else head_ = nullptr;

		for (size_t i = 0; i < array.count_; ++i)
		{
			head_[i] = T(array[i]);
		}
	}

	Array(Array<T>&& array) noexcept
	{
		capacity_ = 0;
		count_ = 0;
		head_ = nullptr;

		array.swap(*this);
	}

	~Array()
	{
		for (int i = count_ - 1; i >= 0; --i)
		{
			head_[i].~T();
		}

		free(head_);
	}

	Array<T>& operator =(const Array<T>& array)
	{
		if (&array == this)
		{
			return *this;
		}

		Array tmp(array);
		tmp.swap(*this);
		return *this;
	}

	Array<T>& operator =(Array<T>&& array) noexcept
	{
		array.swap(*this);
		return *this;
	}

	T& operator [](size_t i)
	{
		return head_[i];
	}

	const T& operator [](size_t i) const
	{
		return head_[i];
	}

	void reserve(size_t size)
	{
		if (size <= capacity_)
		{
			return;
		}

		Array<T> temp(size);

		for (size_t i = 0; i < count_; ++i)
		{
			temp.push_back(head_[i]);
		}

		temp.swap(*this);
	}

	void resize(size_t size)
	{
		Array<T> temp(size);

		for (size_t i = 0; i < count_; ++i)
		{
			temp.push_back(head_[i]);
		}

		for (size_t i = count_; i < size; ++i)
		{
			temp.push_back(T());
		}

		temp.swap(*this);
	}

	void push_back(const T& t)
	{
		if (count_ == capacity_)
		{
			resize(capacity_ + 1);
		}

		new(head_ + count_) T(t);
		++count_;
	}

	void pop_back()
	{
		if (count_ == 0)
		{
			return;
		}

		--count_;
		head_[count_].~T();
	}

	const T& back() const
	{
		return head_[count_ - 1];
	}

	const T& front() const
	{
		return head_[0];
	}

	T& back()
	{
		return head_[count_ - 1];
	}

	T& front()
	{
		return head_[0];
	}

	int size() const
	{
		return capacity_;
	}

	bool empty() const
	{
		return count_ == 0;
	}

	void clear()
	{
		Array<T> empty;
		empty.swap(*this);
	}


	void append(Array<T> array)
	{
		const size_t num_of_items = array.size();

		reserve(num_of_items + count_);

		for (size_t i = 0; i < num_of_items; i++)
		{
			push_back(array[i]);
		}
	}

	void swap(Array<T>& array) noexcept
	{
		const size_t tmp_capacity = array.capacity_;
		const size_t tmp_count = array.count_;
		T* tmp_head = array.head_;

		array.count_ = count_;
		array.capacity_ = capacity_;
		array.head_ = head_;

		count_ = tmp_count;
		capacity_ = tmp_capacity;
		head_ = tmp_head;
	}

private:
	T* head_;
	size_t count_;
	size_t capacity_;
};

